alter table tasks drop constraint FKfv47rw42fro5i9svm8maq22uf
drop table if exists tasks cascade
drop table if exists users cascade
drop sequence if exists task_id_seq
drop sequence if exists user_id_seq