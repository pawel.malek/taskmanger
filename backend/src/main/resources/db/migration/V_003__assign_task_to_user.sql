UPDATE TASKS
SET modify_date = CURRENT_TIMESTAMP,
    user_id     =  1,
    user_uuid   = (SELECT uuid FROM USERS WHERE id = 1)
WHERE id in (1, 2, 3, 5, 11);