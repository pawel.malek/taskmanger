create sequence task_id_seq start 1 increment 1;
create sequence user_id_seq start 1 increment 1;

create table tasks (
    id int8 not null,
    code varchar(25) not null,
    create_date TIMESTAMP not null,
    description_with_format TEXT,
    modify_date TIMESTAMP,
    priority varchar(25) not null,
    status varchar(25) not null,
    title varchar(100) not null,
    user_uuid uuid,
    user_id int8,
    primary key (id)
);

create table users (
    uuid uuid not null,
    id int8 not null,
    create_date TIMESTAMP not null,
    email varchar(200),
    modify_date TIMESTAMP,
    password varchar(255) not null,
    privilege varchar(25) not null,
    username varchar(50) not null,
    primary key (uuid, id)
);

alter table tasks add constraint UK_unique_task_code unique (code);
alter table users add constraint UK_unique_user_email unique (email);
alter table users add constraint UK_unique_username unique (username);
alter table tasks add constraint FK_task_to_user foreign key (user_uuid, user_id) references users;
