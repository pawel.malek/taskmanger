package app;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class MainAppClass {

    private final Logger logger = LogManager.getLogger(MainAppClass.class);

    public static void main(String[] args) {
        SpringApplication.run(MainAppClass.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void afterStartUp() {
        logger.log(Level.INFO, "MainAppClass.afterStartUp - executed");
    }
}
