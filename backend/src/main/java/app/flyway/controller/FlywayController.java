package app.flyway.controller;

import app.flyway.model.FlywayScript;
import app.flyway.service.FlywayScriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@CrossOrigin
@RestController
@RequestMapping("/flyway")
public class FlywayController {

    private final Logger logger = Logger.getLogger(FlywayController.class.getName());
    private FlywayScriptService flywayScriptService;

    @Autowired
    public FlywayController(FlywayScriptService flywayScriptService) {
        this.flywayScriptService = flywayScriptService;
    }

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody List<FlywayScript> getScripts() {
        return this.flywayScriptService.findAll();
    }
}
