package app.flyway.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlywayScript {
    private long id;
    private String name;
    private String version;
    private String description;
    private LocalDateTime executeTime;
    private String type;
}
