package app.flyway.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FlywayScriptMapper implements RowMapper<FlywayScript> {

    @Override
    public FlywayScript mapRow(ResultSet resultSet, int i) throws SQLException {
        return new FlywayScript(
                resultSet.getLong("installed_rank"),
                resultSet.getString("script"),
                resultSet.getString("version"),
                resultSet.getString(("description")),
                resultSet.getTimestamp("installed_on").toLocalDateTime(),
                resultSet.getString("type"));
    }

    /** FIXME : only for tests - other ways to map object from JDBC */
    public static FlywayScript mapRowTestForLambda(ResultSet resultSet, int i) throws SQLException {
        return new FlywayScript(
                resultSet.getLong("installed_rank"),
                resultSet.getString("script"),
                resultSet.getString("version"),
                resultSet.getString(("description")),
                resultSet.getTimestamp("installed_on").toLocalDateTime(),
                resultSet.getString("type"));
    }
}
