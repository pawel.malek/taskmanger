package app.flyway.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource({"classpath:application.properties",
        "classpath:application-postgres.properties"})
public class FlywayConfig {

    @Getter
    private String flywayDatabaseMigrationTable;

    public FlywayConfig(/* FIXME @Value("${spring.flyway.schemas}") String flywayDatabaseSchema,*/
                        @Value("${spring.flyway.table}") String flywayDatabaseTableName) {
        /**
         * FIXME - for test only
         */
        //this.flywayDatabaseMigrationTable = flywayDatabaseSchema + "." + flywayDatabaseTableName;
        this.flywayDatabaseMigrationTable = flywayDatabaseTableName;
    }
}
