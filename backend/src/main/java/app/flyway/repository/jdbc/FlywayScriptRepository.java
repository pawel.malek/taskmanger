package app.flyway.repository.jdbc;

import app.flyway.config.FlywayConfig;
import app.flyway.model.FlywayScript;
import app.flyway.model.FlywayScriptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class FlywayScriptRepository {

    private List<String> flywayBasicFields;
    private JdbcTemplate jdbcTemplate;
    private FlywayConfig flywayConfig;

    @Autowired
    public FlywayScriptRepository(JdbcTemplate jdbcTemplate, FlywayConfig flywayConfig) {
        this.jdbcTemplate = jdbcTemplate;
        this.flywayConfig = flywayConfig;
        this.flywayBasicFields = new ArrayList<>(Arrays.asList(
                "installed_rank",
                "script",
                "version",
                "description",
                "installed_on",
                "type"
        ));
    }

    public FlywayScript findById(Long id) {
        String sql = "SELECT " + String.join(", ", flywayBasicFields)
                + " FROM " + flywayConfig.getFlywayDatabaseMigrationTable()
                + " WHERE installed_rank = ?";

        return jdbcTemplate.queryForObject(sql, new FlywayScriptMapper(), id);  //option #2 - FlywayScriptMapper::mapRowTestForLambda
    }

    public List<FlywayScript> findAll() {
        String sql = "SELECT " + String.join(", ", flywayBasicFields)
                + " FROM " + flywayConfig.getFlywayDatabaseMigrationTable();

        return jdbcTemplate.query(sql, FlywayScriptMapper::mapRowTestForLambda); //option #2 - new FlywayScriptMapper()
    }

}
