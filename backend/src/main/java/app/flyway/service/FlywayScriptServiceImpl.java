package app.flyway.service;

import app.flyway.model.FlywayScript;
import app.flyway.repository.jdbc.FlywayScriptRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlywayScriptServiceImpl implements FlywayScriptService{

    private FlywayScriptRepository flywayScriptRepository;

    @Autowired
    public FlywayScriptServiceImpl(FlywayScriptRepository flywayScriptRepository){
        this.flywayScriptRepository = flywayScriptRepository;
    }

    public FlywayScript findById(Long id){
        return this.flywayScriptRepository.findById(id);
    }

    public List<FlywayScript> findAll(){
        return this.flywayScriptRepository.findAll();
    }
}
