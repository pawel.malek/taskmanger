package app.flyway.service;

import app.flyway.model.FlywayScript;

import java.util.List;

public interface FlywayScriptService {
    FlywayScript findById(Long id);
    List<FlywayScript> findAll();
}
