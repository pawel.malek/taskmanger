package app.task.service;

import app.core.database.crud.BasicCrudMethods;
import app.task.model.entity.Task;
import app.task.model.search.criteria.TaskSearchCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface TaskService extends BasicCrudMethods<Task> {
    Task findByCode(String code);
    List<Task> findAll(Sort sort);
    List<Task> findByUserId(Long userId, Sort sort);
    List<Task> findAllActiveTasksByUserId(Long userId, Sort sort);
    Page<Task> searchTasksBySearchCriteria(Pageable pageableOptions, TaskSearchCriteria taskSearchCriteria);
}
