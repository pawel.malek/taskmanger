package app.task.service;

import app.task.model.entity.Task;
import app.task.model.search.criteria.TaskSearchCriteria;
import app.task.repository.TaskRepository;
import app.user.model.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;

    @Autowired
    public TaskServiceImpl(@Qualifier("taskRepositoryImpl") TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Page<Task> searchTasksBySearchCriteria(Pageable pageableOptions, TaskSearchCriteria taskSearchCriteria) {
        return this.taskRepository.searchTasksBySearchCriteria(pageableOptions, taskSearchCriteria);
    }

    @Override
    public void create(Task task) {
        this.taskRepository.create(task);
    }

    @Override
    public Task getById(long id) {
        return this.taskRepository.getById(id);
    }

    @Override
    public Task findByCode(String code) {
        return this.taskRepository.findByCode(code);
    }

    @Override
    public void update(Task task) {
        this.taskRepository.update(task);
    }

    @Override
    public void delete(Task task) {
        this.taskRepository.delete(task);
    }

    @Override
    public void deleteById(long id) {
        this.taskRepository.deleteById(id);
    }

    @Override
    public List<Task> findAll(Sort sort) {
        return this.taskRepository.findAll(sort);
    }

    @Override
    public List<Task> findByUserId(Long userId, Sort sort) {
        return this.taskRepository.findByUserId(userId, sort);
    }

    @Override
    public List<Task> findAllActiveTasksByUserId(Long userId, Sort sort) {
        return this.taskRepository.findAllActiveTasksByUserId(userId, sort);
    }

}
