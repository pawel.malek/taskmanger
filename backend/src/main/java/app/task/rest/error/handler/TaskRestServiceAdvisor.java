package app.task.rest.error.handler;

import app.task.rest.error.exception.TaskNotFoundException;
import app.core.rest.error.model.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.ZonedDateTime;

@ControllerAdvice
@Slf4j
public class TaskRestServiceAdvisor {

    //@ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({TaskNotFoundException.class})
    public ResponseEntity<Object> handle(HttpServletRequest request, HttpServletResponse response, TaskNotFoundException exception) {
        ErrorResponse errorResponse = new ErrorResponse(request.getRequestURI(), exception.getMessage(),
                /*exception,*/ HttpStatus.NOT_FOUND, ZonedDateTime.now());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(errorResponse);
    }
}

