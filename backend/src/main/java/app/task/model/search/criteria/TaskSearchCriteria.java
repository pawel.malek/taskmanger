package app.task.model.search.criteria;

import app.core.annotation.validation.EnumValidator;
import app.task.model.dictionary.Priority;
import app.task.model.dictionary.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskSearchCriteria implements Serializable {

    private List<@EnumValidator(enumClazz = Status.class) String> status;

    private List<@EnumValidator(enumClazz = Priority.class) String> priority;

    @Min(0)
    private Long userId;

}
