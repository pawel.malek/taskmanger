package app.task.model.dto;

import app.core.annotation.validation.EnumValidator;
import app.task.model.dictionary.Priority;
import app.task.model.dictionary.Status;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class TaskDto implements Serializable {

    private Long id;

    @NotBlank
    @Size(min = 5, max = 25)
    private String code;

    @NotBlank
    @Size(max = 100)
    private String title;

    @Size(max = 2500)
    private String descriptionWithFormat;

    @NotNull
    @EnumValidator(enumClazz = Priority.class)
    private String priority;

    @NotNull
    @EnumValidator(enumClazz = Status.class)
    private String status;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyy HH:mm:ss")
    private LocalDateTime createDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyy HH:mm:ss")
    private LocalDateTime modifyDate;

    public TaskDto(Long id, String code, String title, String descriptionWithFormat, Priority priority, Status status,
                   LocalDateTime createDate, LocalDateTime modifyDate) {
        this.id = id;
        this.code = code;
        this.title = title;
        this.descriptionWithFormat = descriptionWithFormat;
        this.priority = priority.name();
        this.status = status.name();
        this.createDate = createDate;
        this.modifyDate = modifyDate;
    }

}
