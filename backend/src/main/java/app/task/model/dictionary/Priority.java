package app.task.model.dictionary;

public enum Priority {
    CRITICAL,
    IMPORTANT,
    NORMAL,
    MINOR
}
