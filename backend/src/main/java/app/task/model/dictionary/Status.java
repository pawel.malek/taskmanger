package app.task.model.dictionary;

public enum Status {
    CREATED,
    TODO,
    SUSPENDED,
    IN_PROGRESS,
    FINISHED
}
