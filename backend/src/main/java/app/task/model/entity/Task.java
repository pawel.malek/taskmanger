package app.task.model.entity;

import app.task.model.dictionary.Priority;
import app.task.model.dictionary.Status;
import app.user.model.entity.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity
@Table(name = "tasks")
public class Task implements Serializable {//} extends EntityWithUUID {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "task_id_seq")
    @SequenceGenerator(name = "task_id_seq", sequenceName = "task_id_seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @Column(name = "code", unique = true, nullable = false, length = 25)
    private String code;

    @Column(name = "title", nullable = false, length = 100)
    private String title;

    @Column(name = "description_with_format", columnDefinition="TEXT")
    private String descriptionWithFormat;

    @Enumerated(EnumType.STRING)
    @Column(name = "priority", nullable = false, length = 25)
    private Priority priority;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, length = 25)
    private Status status;

    @Column(name = "create_date", columnDefinition = "TIMESTAMP", nullable = false)
    private LocalDateTime createDate;

    @Column(name = "modify_date", columnDefinition = "TIMESTAMP")
    private LocalDateTime modifyDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name="user_id", referencedColumnName="id"),
            //this one is not needed but added for test
            @JoinColumn(name="user_uuid", referencedColumnName="uuid")
    })
    private User user;

    public Task(Long id, String code, String title, String descriptionWithFormat, Priority priority, Status status,
                LocalDateTime createDate, LocalDateTime modifyDate) {
        this.id = id;
        this.code = code;
        this.title = title;
        this.descriptionWithFormat = descriptionWithFormat;
        this.priority = priority;
        this.status = status;
        this.createDate = createDate;
        this.modifyDate = modifyDate;
    }

}
