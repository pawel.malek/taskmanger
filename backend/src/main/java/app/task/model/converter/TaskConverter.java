package app.task.model.converter;

import app.task.model.dictionary.Priority;
import app.task.model.dictionary.Status;
import app.task.model.entity.Task;
import app.task.model.dto.TaskDto;

public class TaskConverter {

    public static Task toTask(TaskDto taskDto) {
        return new Task(taskDto.getId(), taskDto.getCode(), taskDto.getTitle(), taskDto.getDescriptionWithFormat(),
                Priority.valueOf(taskDto.getPriority()),  Status.valueOf(taskDto.getStatus()),
                taskDto.getCreateDate(), taskDto.getModifyDate());
    }

    public static TaskDto toTaskDto(Task task) {
        return new TaskDto(task.getId(), task.getCode(), task.getTitle(), task.getDescriptionWithFormat(),
                task.getPriority(), task.getStatus(),
                task.getCreateDate(), task.getModifyDate());
    }
}
