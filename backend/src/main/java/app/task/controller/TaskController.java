package app.task.controller;

import app.task.model.converter.TaskConverter;
import app.task.model.dictionary.Priority;
import app.task.model.dictionary.Status;
import app.task.model.dto.TaskDto;
import app.task.model.entity.Task;
import app.task.model.search.criteria.TaskSearchCriteria;
import app.task.rest.error.exception.TaskNotFoundException;
import app.task.service.TaskService;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Validated
@CrossOrigin
@RestController
@RequestMapping("/tasks")
public class TaskController {

    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<TaskDto> getAllTasks() {
        Sort sortOptions = Sort.by(Sort.Direction.DESC, "createDate", "code");
        List<Task> tasks = taskService.findAll(sortOptions);

        return tasks.stream()
                .map(TaskConverter::toTaskDto)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<TaskDto>> getAllUserTasks(@Positive @PathVariable(value = "id") Long id,
                                                         @JsonDeserialize(using = NumberDeserializers.BooleanDeserializer.class)
                                                         @RequestParam(value = "active", defaultValue = "true") Boolean active) {
        Sort sortOptions = Sort.by(Sort.Direction.DESC, "createDate", "code");
        List<Task> tasks;

        if (active) {
            tasks = taskService.findAllActiveTasksByUserId(id, sortOptions);
        }
        else {
            tasks = taskService.findByUserId(id, sortOptions);
        }

        return ResponseEntity.ok(tasks.stream()
                .map(TaskConverter::toTaskDto)
                .collect(Collectors.toList()));
    }

    @GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<TaskDto>> searchTasks(@Valid TaskSearchCriteria taskSearchCriteria,
                                                                   @RequestParam(value = "offset", required = false, defaultValue = "0") Integer offset,
                                                                   @RequestParam(value = "limit", required = false, defaultValue = "10") Integer limit) {
        Sort sortOptions = Sort.by(Sort.Direction.DESC, "createDate", "code");
        Pageable pageableOptions = PageRequest.of(offset, limit, sortOptions);
        Page<Task> pageableTasks = taskService.searchTasksBySearchCriteria(pageableOptions, taskSearchCriteria);

        List<TaskDto> taskDtoList = pageableTasks.getContent().stream()
                .map(TaskConverter::toTaskDto)
                .collect(Collectors.toList());

        HttpHeaders responseHeaders = preparePageableHeaders(pageableTasks);

        return new ResponseEntity<>(taskDtoList, responseHeaders, HttpStatus.OK);
    }

    protected HttpHeaders preparePageableHeaders(Page page) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("pageable-total-elements", String.valueOf(page.getTotalElements()));
        responseHeaders.set("pageable-total-pages", String.valueOf(page.getTotalPages()));
        responseHeaders.set("pageable-page-number", String.valueOf(page.getNumber()));
        responseHeaders.set("pageable-number-of-elements", String.valueOf(page.getNumberOfElements()));

        return responseHeaders;
    }

    @GetMapping("/{code}")
    @ResponseStatus(HttpStatus.OK)
    public TaskDto getTasksByCode(@PathVariable String code) {
        return TaskConverter.toTaskDto(taskService.findByCode(code));
    }

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void createTask(@RequestBody @Valid TaskDto taskDto){
        Task task = TaskConverter.toTask(taskDto);
        task.setId(null);
        task.setCreateDate(LocalDateTime.now());
        task.setModifyDate(null);
        this.taskService.create(task);
    }

    @PutMapping("/update")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateTask(@RequestBody TaskDto taskDto){
        Task task = this.taskService.getById(taskDto.getId());
        task.setTitle(taskDto.getTitle());
        task.setDescriptionWithFormat(taskDto.getDescriptionWithFormat());
        task.setPriority(Priority.valueOf(taskDto.getPriority()));
        task.setStatus(Status.valueOf(taskDto.getStatus()));
        task.setModifyDate(LocalDateTime.now());
        this.taskService.update(task);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteTask(@Positive @PathVariable(value = "id") Long id) {
        Task task = this.taskService.getById(id);
        this.taskService.delete(task);
    }

    @PatchMapping("/patch")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void patchTask(@RequestBody TaskDto taskDto){
        Task task = this.taskService.getById(taskDto.getId());
        if (StringUtils.isNoneEmpty(taskDto.getTitle())) {
            task.setTitle(taskDto.getTitle());
        }
        if (StringUtils.isNoneEmpty(taskDto.getTitle())) {
            task.setDescriptionWithFormat(taskDto.getTitle());
        }
        if (taskDto.getPriority() != null) {
            task.setPriority(Priority.valueOf(taskDto.getPriority()));
        }
        if (taskDto.getStatus() != null) {
            task.setStatus(Status.valueOf(taskDto.getStatus()));
        }
        task.setModifyDate(LocalDateTime.now());
        this.taskService.update(task);
    }

    /** FIXME : to remove - used only for tests */
    @GetMapping("/id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public TaskDto getTasksByIdPathVariable(@PathVariable long id) {
        return TaskConverter.toTaskDto(taskService.getById(id));
    }

    /** FIXME : to remove - used only for tests */
    @GetMapping("/id")
    @ResponseStatus(HttpStatus.OK)
    public TaskDto getTasksByIdRequestParam(@RequestParam("id") long id) {
        return TaskConverter.toTaskDto(taskService.getById(id));
    }

    /** FIXME : to remove - used only for tests */
    @GetMapping("/byId-test/{id}")
    public ResponseEntity<TaskDto> getTaskByIdTest(HttpServletRequest request, @PathVariable("id") long id) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Response-Header", "data in response header");

        TaskDto task;
        try {
            task = TaskConverter.toTaskDto(taskService.getById(id));
        } catch (EntityNotFoundException ex){
            throw new TaskNotFoundException("task not found, id: " + id);
        }

        return new ResponseEntity<>(task, responseHeaders, HttpStatus.OK);
    }
}
