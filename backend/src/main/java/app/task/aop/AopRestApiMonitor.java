package app.task.aop;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AopRestApiMonitor {

    private final Logger logger = LogManager.getLogger(AopRestApiMonitor.class);

    @Before("execution(* app.task.controller.TaskController.*(..))")
    public void logBeforeProductEvents(JoinPoint joinPoint) {
        String method = "TaskController." + joinPoint.getSignature().getName();
        logger.log(Level.INFO, "***** AOP - LOGGER - " + method + " *****");
    }

}
