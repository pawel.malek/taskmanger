package app.task.repository;

import app.task.model.dictionary.Priority;
import app.task.model.dictionary.Status;
import app.task.model.entity.Task;
import app.task.model.search.criteria.TaskSearchCriteria;
import app.task.repository.custom.jpa.CustomJpaTaskRepository;
import app.task.repository.spring.data.SpringDataTaskRepository;
import app.task.rest.error.exception.TaskNotFoundException;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class TaskRepositoryImpl implements TaskRepository {

    private SpringDataTaskRepository springDataTaskRepository;
    private CustomJpaTaskRepository customJpaTaskRepository;

    @Autowired
    public TaskRepositoryImpl(SpringDataTaskRepository repository, CustomJpaTaskRepository customJpaTaskRepository){
        this.springDataTaskRepository = repository;
        this.customJpaTaskRepository = customJpaTaskRepository;
    }

    @Override
    @CachePut(value = "userActiveTasks", key = "#task.id")
    public void create(Task task) {
        this.springDataTaskRepository.save(task);
    }

    @Override
    public Task getById(long id) {
        return this.springDataTaskRepository.getById(id);
    }

    @Override
    public Task findByCode(String code){
        return this.springDataTaskRepository.findByCode(code);
    }

    @Override
    //@CachePut(value = "userTasks", key = "#task.id")
    @CacheEvict(value = "userActiveTasks", key = "#task.id", allEntries = true)
    public void update(Task task) {
        if(this.springDataTaskRepository.existsById(task.getId())) {
            this.springDataTaskRepository.save(task);
        }
        else {
            throw new TaskNotFoundException("task not found, id: " + task.getId());
        }
    }

    @Override
    @CacheEvict(value = "userActiveTasks", key = "#task.id")
    public void delete(Task task) {
        this.springDataTaskRepository.delete(task);
    }

    @Override
    @CacheEvict(value = "userActiveTasks", key = "#task.id")
    public void deleteById(long id) {
        this.springDataTaskRepository.deleteById(id);
    }

    @Override
    public List<Task> findAll(Sort sort) {
        return this.springDataTaskRepository.findAll(sort);
    }

    @Override
    public List<Task> findByUserId(Long userId, Sort sort) {
        return this.springDataTaskRepository.findByUserId(userId, sort);
    }

    @Override
    @Cacheable(cacheNames = "userActiveTasks")
    public List<Task> findAllActiveTasksByUserId(Long userId, Sort sort) {
        return this.customJpaTaskRepository.findActiveTaskByUserIdCriteria(userId, sort);
    }

    @Override
    public Page<Task> searchTasksBySearchCriteria(Pageable pageableOptions, TaskSearchCriteria taskSearchCriteria) {
        return this.springDataTaskRepository.searchTasks(taskSearchCriteria.getUserId(),
                CollectionUtils.emptyIfNull(taskSearchCriteria.getStatus())
                        .stream()
                        .map(Status::valueOf)
                        .collect(Collectors.toList()),
                CollectionUtils.emptyIfNull(taskSearchCriteria.getPriority())
                        .stream()
                        .map(Priority::valueOf)
                        .collect(Collectors.toList()),
                pageableOptions);
    }
}
