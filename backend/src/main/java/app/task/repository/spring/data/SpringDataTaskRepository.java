package app.task.repository.spring.data;

import app.task.model.dictionary.Priority;
import app.task.model.dictionary.Status;
import app.task.model.entity.Task;
import app.user.model.entity.User;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.web.PageableDefault;

import java.util.List;

public interface SpringDataTaskRepository extends JpaRepository<Task, Long> {

    Task findByCode(String code);

    List<Task> findByUserId(Long id, Sort sort);

    @Modifying
    @Query("UPDATE Task t " +
            "SET t.title = :title, t.modifyDate = CURRENT_TIMESTAMP " +
            "WHERE t.id = :id")
    int updateTaskTitle(@Param("id") long id,
                        @Param("title") Status status);

    @Modifying
    @Query("UPDATE Task t " +
            "SET t.status = :status, t.modifyDate = CURRENT_TIMESTAMP " +
            "WHERE t.id = :id")
    int updateTaskStatus(@Param("id") long id,
                         @Param("status") Status status);

    @Modifying
    @Query("UPDATE Task t " +
            "SET t.priority = :priority, t.modifyDate = CURRENT_TIMESTAMP " +
            "WHERE t.id = :id")
    int updateTaskPriority(@Param("id") long id,
                           @Param("priority") Priority priority);

    @Modifying
    @Query("UPDATE Task t " +
            "SET t.descriptionWithFormat = :descriptionWithFormat, t.modifyDate = CURRENT_TIMESTAMP " +
            "WHERE t.id = :id")
    int updateTaskDescription(@Param("id") long id,
                              @Param("descriptionWithFormat") String descriptionWithFormat);

    @Query("SELECT t FROM Task t " +
            "LEFT JOIN FETCH User u ON u = t.user  " +
            "WHERE (:userId IS NULL OR u.id = :userId) " +
            "AND (COALESCE(:statusList) IS NULL OR t.status IN :statusList) " +
            "AND (COALESCE(:priorityList) IS NULL OR t.priority IN :priorityList)")
    Page<Task> searchTasks(@Param("userId") Long userId,
                          @Param("statusList") List<Status> status,
                          @Param("priorityList") List<Priority> priority,
                          @PageableDefault(size = 10, page = 0,
                                  sort = {"id"}, direction = Sort.Direction.ASC) Pageable pageable);
}
