package app.task.repository.custom.jpa;

import app.task.model.entity.Task;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface CustomJpaTaskRepository {
    List<Task> findActiveTaskByUserIdJpql(Long userId, Sort sort);
    List<Task> findActiveTaskByUserIdCriteria(Long userId, Sort sort);
}
