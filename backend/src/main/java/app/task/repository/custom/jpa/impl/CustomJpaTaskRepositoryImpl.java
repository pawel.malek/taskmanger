package app.task.repository.custom.jpa.impl;

import app.task.model.dictionary.Priority;
import app.task.model.dictionary.Status;
import app.task.model.entity.Task;
import app.task.repository.custom.jpa.CustomJpaTaskRepository;
import app.task.repository.custom.jpa.specification.TaskWithPriority;
import app.task.repository.custom.jpa.specification.TaskWithStatus;
import app.user.model.entity.User;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;

import java.util.List;

@Repository
public class CustomJpaTaskRepositoryImpl implements CustomJpaTaskRepository {

    @PersistenceContext
    private EntityManager entityManager;

    private final List<Status> activeStatuses = List.of(
                Status.CREATED,
                Status.IN_PROGRESS,
                Status.TODO
    );

    @Override
    @Transactional
    public List<Task> findActiveTaskByUserIdJpql(Long userId, Sort sort) {
        String selectQueryJpql = "SELECT task FROM Task task WHERE task.user.id = :userId AND task.status IN :activeStatuses";
        Query query = entityManager.createQuery(selectQueryJpql, Task.class);
        query.setParameter("userId", userId);
        query.setParameter("activeStatuses", activeStatuses);
        return query.getResultList();
    }

    /**
     * FIXME
     * This method is only for experimental reasons as alternative for JPQL
     * tips - https://thorben-janssen.com/hibernate-tip-left-join-fetch-join-criteriaquery/
     * */
    @Override
    public List<Task> findActiveTaskByUserIdCriteria(Long userId, Sort sort) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Task> criteriaQuery = criteriaBuilder.createQuery(Task.class);

        Root<Task> taskRoot = criteriaQuery.from(Task.class);
        Join<User, Task> userRoot = taskRoot.join("user", JoinType.LEFT);  //<Child, Parent>

        //FIXME - For test only
       // Fetch<Object, Object> userRootFetch = taskRoot.fetch("user", JoinType.LEFT);  //<Child, Parent>

        Predicate predicateForUserId = criteriaBuilder.equal(userRoot.get("id"), userId);
        Predicate predicateForStatus = criteriaBuilder.in(taskRoot.get("status")).value(activeStatuses);
        Predicate finalPredicate = criteriaBuilder.and(predicateForUserId, predicateForStatus);

        criteriaQuery.select(taskRoot).where(finalPredicate);

        return entityManager.createQuery(criteriaQuery).getResultList();
    }

}
