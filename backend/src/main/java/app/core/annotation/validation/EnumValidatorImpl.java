package app.core.annotation.validation;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraintvalidation.SupportedValidationTarget;
import javax.validation.constraintvalidation.ValidationTarget;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SupportedValidationTarget(ValidationTarget.ANNOTATED_ELEMENT)
public class EnumValidatorImpl implements ConstraintValidator<EnumValidator, String> {

    private Class<? extends Enum<?>> enumClazz;

    @Override
    public void initialize(EnumValidator constraintAnnotation) {
       enumClazz = constraintAnnotation.enumClazz();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (enumClazz.isEnum()) {
            List<String> allowedValues = Arrays.stream(enumClazz.getEnumConstants())
                    .map(String::valueOf)
                    .collect(Collectors.toList());

            if (allowedValues.contains(value)) {
                return true;
            }
        }
        setMessageParameters(context, Map.of("value", value, "enum", enumClazz.getSimpleName()));
        /* FIXME : not used - only for tests */
        //setCustomMessage(context, "Overridden message");

        return false;
    }

    /* FIXME : not used - only for tests */
    private void setCustomMessage(ConstraintValidatorContext constraintContext, String message) {
        constraintContext.buildConstraintViolationWithTemplate(message).addConstraintViolation();
    }

    private void setMessageParameters(ConstraintValidatorContext constraintContext, Map<String, String> parameters) {
        HibernateConstraintValidatorContext hibernateConstraintValidatorContext = constraintContext.unwrap( HibernateConstraintValidatorContext.class );

        for (Map.Entry<String, String> entry: parameters.entrySet()) {
            hibernateConstraintValidatorContext.addMessageParameter(entry.getKey(), entry.getValue());
        }
    }
}
