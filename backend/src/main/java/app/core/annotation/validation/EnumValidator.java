package app.core.annotation.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = EnumValidatorImpl.class)
@Target( { ElementType.METHOD, ElementType.FIELD, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface EnumValidator {
    String message() default "Value: \'{value}\' not found in Enum: \'{enum}\'";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    //Custom
    Class<? extends Enum<?>> enumClazz();
}