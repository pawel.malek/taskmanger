package app.core.rest.error.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

@Getter
public class ErrorResponse {
    private final String path;
    private final String message;
    //private final Throwable throwable;
    private final HttpStatus httpStatus;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private final ZonedDateTime timestamp;

    public ErrorResponse(String path, String message, /*Throwable throwable,*/ HttpStatus httpStatus, ZonedDateTime timestamp) {
        this.path = path;
        this.message = message;
        //this.throwable = throwable;
        this.httpStatus = httpStatus;
        this.timestamp = timestamp;
    }
}
