package app.core.database.aop;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Aspect
@Component
@DependsOn({"cacheInitializer"})
public class AopCacheMonitor {

    private final Logger logger = LogManager.getLogger(AopCacheMonitor.class);

    @After("execution(* app.core.database.cache.CacheInitializer.*(..))")
    public void logAfterCacheInitialize(JoinPoint joinPoint) {
        String method = "CacheInitializer." + joinPoint.getSignature().getName();
        logger.log(Level.INFO, "***** AOP - LOGGER - " + method + " *****");
    }
}
