package app.core.database.model;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@MappedSuperclass
public class EntityWithUUID {

    @Id
    @Type(type = "pg-uuid")
    @Column(name = "id", length = 16, unique = true, nullable = false)
    private UUID uuid;

    public EntityWithUUID() {
        this.uuid = UUID.randomUUID();
    }
}
