package app.core.database.cache;

import app.task.repository.TaskRepository;
import app.task.repository.TaskRepositoryImpl;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component(value = "cacheInitializer")
public class CacheInitializer {

    private final Logger logger = LogManager.getLogger(CacheInitializer.class);

    ApplicationContext context;

    @Autowired
    public CacheInitializer(ApplicationContext context) {
        logger.log(Level.INFO, "CacheInitializer - new");
        this.context = context;
        initialize();
    }

    public void initialize() {
        Set<Method> methods = Arrays.stream(TaskRepositoryImpl.class.getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(Cacheable.class))
                .collect(Collectors.toSet());

        for (Method method : methods) {
            try {
                /** FIXME
                 * currently its for test only - for findByUserId(id, sort)
                 */
                Collection cachedItems = (Collection) method.invoke(context.getBean(TaskRepository.class),
                        1L, Sort.by(Sort.Direction.DESC, "createDate", "code"));

                logger.log(Level.INFO, "CacheInitializer - " + method.getName() + ", items: " + cachedItems.size());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

}
