package app.core.database.cache;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableCaching
@Configuration
public class EmbeddedCacheConfig  {

    @Bean
    Config config() {
        Config config = new Config();
        MapConfig mapConfig = new MapConfig();

        mapConfig.setTimeToLiveSeconds(300);
        config.getMapConfigs().put("userActiveTasks", mapConfig);

        return config;
    }

}
