package app.core.database.crud;

import app.task.model.entity.Task;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface BasicCrudMethods<T> {
    void create(T t);
    T getById(long id);
    void update(T t);
    void delete(T t);
    void deleteById(long id);
    List<Task> findAll(Sort sort);
}
