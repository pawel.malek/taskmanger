package app.user.model.entity;

import app.task.model.dictionary.Status;
import app.task.model.entity.Task;
import app.user.model.dictionary.Privilege;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @Type(type = "pg-uuid")
    private UUID uuid;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_seq")
    @SequenceGenerator(name = "user_id_seq", sequenceName = "user_id_seq", initialValue = 1, allocationSize = 1)
    private Long id;

    @Column(name = "username", unique = true, nullable = false, length = 50)
    private String username;

    @Column(name = "password", nullable = false)
    private Character[] password;

    @Email
    @Column(name = "email", unique = true, length = 200)
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "privilege", nullable = false, length = 25)
    private Privilege privilege;

    @Column(name = "create_date", columnDefinition = "TIMESTAMP", nullable = false)
    private LocalDateTime createDate;

    @Column(name = "modify_date", columnDefinition = "TIMESTAMP")
    private LocalDateTime modifyDate;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "user")
    private List<Task> tasks;

    public User() {
        this.uuid = UUID.randomUUID();
    }

}
