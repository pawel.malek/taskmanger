package app.user.model.dictionary;

public enum Privilege {
    READONLY,
    STANDARD,
    ADMIN
}
