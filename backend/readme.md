# **Task Manager** 
###### **Application used to manage work and jobs to do.**

### Tech Stack:
###### Backend
```
Java 16
Spring Boot
Spring REST 
Sprind Data JPA/JDBC
Flyway
Postgres
...
```
###### Frontend
```
Angular ... ?
```

### Swagger doc:
```
- http://www.localhost:8080/api/swagger-ui/index.html?configUrl=/api/v3/api-docs/swagger-config
- http://www.localhost:8880/api/v2/api-docs
```

###To be continued...






